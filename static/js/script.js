var pushId = "web.org.toolforge";
var domain = "https://push-notifications-helper.toolforge.org";

var subscribe = document.querySelector(".subscribe");
subscribe.addEventListener(
  "click",
  function (evt) {
    pushNotification();
  },
  false
);

var pushNotification = function () {
  if ("safari" in window && "pushNotification" in window.safari) {
    var permissionData = window.safari.pushNotification.permission(pushId);
    checkRemotePermission(permissionData);
  } else {
    alert("Push notifications not supported.");
  }
};

var checkRemotePermission = function (permissionData) {
  console.log("Permission data:" + JSON.stringify(permissionData));
  if (permissionData.permission === "default") {
    console.log("The user is making a decision");
    window.safari.pushNotification.requestPermission(
      domain,
      pushId,
      {},
      checkRemotePermission
    );
  } else if (permissionData.permission === "denied") {
    console.dir(arguments);
  } else if (permissionData.permission === "granted") {
    console.log("The user said yes, with token: " + permissionData.deviceToken);
    let elem = document.getElementById("token");
    elem.textContent = `Device token: ${permissionData.deviceToken}`;
  }
};
