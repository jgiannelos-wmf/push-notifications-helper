# Push notifications helper

## Installation
```
> npm install
> npm start
```

## Supported browsers

* Safari

## Configuration
### Prepare safari push package

```
> # Requires ruby
> gem install push_package
> cat push.rb
require 'push_package'

# Safari push package doc
# https://developer.apple.com/library/archive/documentation/NetworkingInternet/Conceptual/NotificationProgrammingGuideForWebsites/PushNotifications/PushNotifications.html
website_params = {
  websiteName: "__REPLACE_ME__",
  websitePushID: "__REPLACE_ME__",
  allowedDomains: [
    "__REPLACE_ME__"
  ],
  urlFormatString: ""__REPLACE_ME__"",
  authenticationToken: "__REPLACE_ME__",
  webServiceURL: "__REPLACE_ME__"
}

iconset_path = '/app/icon.iconset'
certificate = '/app/cert.p12'
intermediate_cert = '/app/AppleWWDRCA.cer'
package = PushPackage.new(website_params, iconset_path, certificate, '', intermediate_cert)
package.save('package.zip')

> ruby push.rb
```
