var express = require("express");
var bodyParser = require("body-parser");
var morganBody = require("morgan-body");

var app = express();
var port = process.env.PORT || 3000;

const pushPackageRoute =
  "/v*/pushPackages/web.org.toolforge";

app.use(bodyParser.json());
morganBody(app, { logAllReqHeader: true, logAllResHeader: true });

app.post(pushPackageRoute, function (req, res) {
  res.sendfile("package.zip", {
    headers: { "Content-type": "application/zip" },
  });
});

app.post("/v*/log", function (req, res) {
  res.json(200, { status: "ok" });
});

app.listen(port);
